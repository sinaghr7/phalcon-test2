<?php

namespace MyApp\Models\Assessment;

use Phalcon\Mvc\Model;

class Assessment extends Model
{
    public $id;
    public $assessment_id;

    public function initialize()
    {
        $this->belongsTo('id' , 'Services' ,'assessment_id', array(
            'alias' => 'service'
        ));
    }
}