<?php
namespace MyApp\Models\Services;

use MyApp\Models\Assessment\Assessment;
use Phalcon\Mvc\Model;

class Services extends Model
{
    public $id;
    
    public function initialize()
    {
        $this->hasMany('id' , 'Users' , 'id');
        $this->hasOne('assessment_id', Assessment::class, 'id', array(
            'alias' => 'Assessment'
        ));
    }
}