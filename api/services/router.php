<?php
use Phalcon\Mvc\Micro\Collection as MicroCollection;

return call_user_func(function(){
    $services = new MicroCollection();
    $services->setHandler(new \MyApp\Api\ServiceController\Controller());
    $services->setPrefix('/services');
    $services->get('/', 'getServices');
    $services->get('/{id}', 'getService');
    $services->post('/', 'createService');
    $services->put('/{id}', 'updateService');
    $services->delete('/{id}' ,'deleteService'); 
    return $services;
});