<?php

namespace MyApp\Api\UserController;

use MyApp\Models\Users\Users;
use Phalcon\Crypt;
use Phalcon\Di\Injectable;

class Controller extends Injectable
{
    public function getUsers()
    {
        $users = Users::find();
        if (!$users) {
            $this->response->setStatusCode(404);
            $this->response->setContent("Oops.. we cant find any user!");
            return $this->response;
        } else {
            $this->response->setJsonContent(
                $users->toArray()
            );
            return $this->response;
        }
    }

    public function getUser($id)
    {
        $user = Users::findFirst($id);
        if (!$user) {
            $this->response->setStatusCode(404);
            $this->response->setContent("Oops.. we cant find any user with this id!");
            return $this->response;
        } else {
            $this->response->setJsonContent($user);
            return $this->response;
        }
    }

    public function registerUser()
    {
        $data = $this->request->getJsonRawBody(true);
        $user = new Users();
        $user->userName = $data["userName"];
        $user->password = $this->security->hash($data["password"]);
        $user->FirstName = $data["FirstName"];
        $user->LastName = $data["LastName"];
        $user->role = "Admins";
        $crypt = new Crypt();
        $hash = array(
            "userName" => $data["userName"]
        );
        $key = "secret";
        $text = json_encode($hash);
        $user->token = $crypt->encryptBase64($text, $key);
        $token[] = $user->token;
        if ($user->create() === false) {
            $messages = $user->getMessages();
            foreach ($messages as $message) {
                $items[] = $message->getMessage();
            }
            $this->response->setStatusCode(406);
            $this->response->setJsonContent($items);
            return $this->response;
        } else {
            $this->response->setContent("user registered successfully");
            $this->response->setJsonContent($token);
            return $this->response;
        }
    }

    public function loginUser()
    {
        $data = $this->request->getJsonRawBody(true);
        $user = Users::findFirst([
            "conditions" => "userName = :userName:",
            "bind" => [
                "userName" => $data["userName"]
            ],
        ]);
        if ($user) {
            if ($this->security->checkHash($data["password"], $user->password)) {
                $hash = array(
                    "userName" => $user->userName
                );
                $crypt = new Crypt();
                $key = "secret";
                $text = json_encode($hash);
                $user->token = $crypt->encryptBase64($text, $key);
                $token[] = $user->token;
                $user->update();
                $this->response->setJsonContent($token);
                return $this->response;
            } else {
                $this->response->setContent("Incorrect username");
                return $this->response;
            }
        }
    }

    public function updateUser($id)
    {
        $data = $this->request->getJsonRawBody(true);
        $user = Users::findFirst($id);
        $user->userName = $data["userName"];
        $user->password = $this->security->hash($data["password"]);
        $user->firstName = $data["firstName"];
        $user->lastName = $data["lastName"];
        if ($user->update() === false) {
            $messages = $user->getMessages();
            foreach ($messages as $message) {
                $items[] = $message->getMessage();
            }
            $this->response->setStatusCode(406);
            $this->response->setJsonContent($items);
            return $this->response;
        } else {
            $this->response->setContent("user updated successfully");
            return $this->response;
        }
    }

    public function deleteUser($id)
    {
        $user = Users::findFirst($id);
        if (!$user) {
            $this->response->setStatusCode(404);
            $this->response->setContent("Oops.. we cant find any user with this id!");
            return $this->response;
        } else {
            $user->delete();

            $this->response->setContent("user deleted successfully");
            return $this->response;
        }
    }
}