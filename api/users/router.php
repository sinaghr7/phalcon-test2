<?php
use Phalcon\Mvc\Micro\Collection as MicroCollection;

return call_user_func(function(){
    $users = new MicroCollection();
    $users->setHandler(new \MyApp\Api\UserController\Controller());
    $users->setPrefix('/users');
    $users->get('/', 'getUsers');
    $users->get('/{id}', 'getUser');
    $users->post('/register', 'registerUser');
    $users->post('/login', 'loginUser');
    $users->put('/{id}', 'updateUser');
    $users->delete('/{id}' ,'deleteUser'); 
    return $users;
});