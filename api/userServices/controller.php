<?php

namespace MyApp\Api\userServicesController;

use MyApp\Models\Services\Services;
use MyApp\Models\UserServices\Userservices;
use MyApp\Models\Assessment\Assessment;
use Phalcon\Di\Injectable;

class Controller extends Injectable
{
    public function getUserServices()
    {
        $userServices = Userservices::find();
        if (!$userServices) {
            $this->response->setStatusCode(404);
            $this->response->setContent("Oops.. we cant find any userServices!");
            return $this->response;
        } else {
            $this->response->setJsonContent(
                $userServices->toArray()
            );
            return $this->response;
        }
    }

    public function getUserService($id)
    {
        $userService = Userservices::findFirst($id);
        if (!$userService) {
            $this->response->setStatusCode(404);
            $this->response->setContent("Oops.. we cant find any userService with this id!");
            return $this->response;
        } else {
            $this->response->setJsonContent($userService);
            return $this->response;
        }
    }

    public function createUserServices()
    {
        $data = $this->request->getJsonRawBody(true);
        $userService = new Userservices();
        $userService->user_id = $data["user_id"];
        $userService->service_id = $data["service_id"];
        $userService->status = $data["status"];
        $userService->start = $data["start"];
        $userService->finish = $data["finish"];
        if ($userService->create() === false) {
            $this->response->setStatusCode(406);
            $this->response->setContent("we cant create userService");
            return $this->response;
        } else {
            $this->response->setContent("UserService created successfully");
            return $this->response;
        }
    }

    public function updateUserServices($id)
    {
        $data = $this->request->getJsonRawBody(true);
        $userService = Userservices::findFirst($id);
        $userService->user_id = $data["user_id"];
        $userService->service_id = $data["service_id"];
        $userService->status = $data["status"];
        $userService->start = $data["start"];
        $userService->finish = $data["finish"];
        if ($userService->update() === false) {
            $this->response->setStatusCode(404);
            $this->response->setContent("we cant update UserService");
            return $this->response;
        } else {
            $this->response->setContent("UserService updated successfully");
            return $this->response;
        }
    }

    public function deleteUserServices($id)
    {

        $userService = Userservices::findFirst($id);
        if (!$userService) {
            $this->response->setStatusCode(404);
            $this->response->setContent("Oops.. we cant find any UserService with this id!");
            return $this->response;
        } else {
            $userService->delete();
            $this->response->setContent("UserService deleted successfully");
            return $this->response;
        }
    }

    public function getAssessment($id)
    {
        $service = Services::findFirst([
            "conditions" => "assessment_id = :assessment_id:",
            "bind" => [
                "assessment_id" => $id,
            ]
        ]);
        $assessment = $service->Assessment;
        $assessment->structure = json_decode($assessment->structure);
        for ($i = 0; $i < 60; $i++) {
            $help[$i]["number"] = $assessment->structure[$i]->{'number'};
            $help[$i]["question"] = $assessment->structure[$i]->{'question'};
            $help1[$i][0] = $assessment->structure[$i]->{'answer'}[0]->{'0'};
            $help1[$i][1] = $assessment->structure[$i]->{'answer'}[1]->{'0'};
            $help[$i]["answer"] = $help1[$i];
        }
        if (!$assessment) {
            $this->response->setStatusCode(404);
            $this->response->setContent("Oops.. we cant find Assessment!");
            return $this->response;
        } else {
            $this->response->setJsonContent(
                $help
            );
            return $this->response;
        }
    }

    public function createAssessment()
    {
        $dir = dirname(__FILE__);
        $lines = file($dir . '/test.txt');
        $string = str_replace(array("\r", "\n", "\t"), '', $lines);
        foreach ($string as $line_num) {
            $test[] = $line_num;
            $lastChar[] = substr($line_num, -1);
        }
        for ($i = 0; $i < 120; $i++) {
            $test[$i] = str_replace($lastChar[$i], '', $string[$i]);
        }
        for ($x = 0, $z = 1; $x < 120; $x = $x + 2, $z++) {
            $y = $x + 1;
            $array["number"] = $z;
            $array["question"] = "کدام گزینه را انتخاب میکنید ؟";
            $help1[] = $test[$x];
            $help2[] = $test[$y];
            $help3 [0] = $help1;
            $help3 [0]["index"] = $lastChar[$x];
            $help3 [1] = $help2;
            $help3 [1]["index"] = $lastChar[$x + 1];
            $array["answer"] = $help3;
            $result[] = $array;
            unset($array, $help1, $help2, $help3);
        }
        $example = json_encode($result);
        $assessment = new Assessment();
        $assessment->structure = $example;
        if ($assessment->create() === false) {
            $this->response->setStatusCode(404);
            $this->response->setContent("we cant create Assessment");
            return $this->response;
        } else {
            $this->response->setJsonContent($result);
            return $this->response;
        }
    }

    public function answerAssessment()
    {
        $data = $this->request->getJsonRawBody(true);
        $result = $data["answer"];
        $userService = Userservices::findFirst([
            "conditions" => "user_id = :user_id: AND id = :id:",
            "bind" => [
                "user_id" => $this->di->get("userObject")->id,
                "id" => $result[0]["userServiceId"],
            ]
        ]);
        if ($userService == true) {
            $service = Services::findFirst([
                "conditions" => "id = :id:",
                "bind" => [
                    "id" => $userService->service_id,
                ]
            ]);
            $assessment = $service->Assessment;
            $assessment->structure = json_decode($assessment->structure);
            if ($userService->result == true) {
                $this->response->setStatusCode(406);
                $this->response->setContent("you have complete this assessment before");
                return $this->response;
            } else {
                $answer["e"] = 0;$answer["f"] = 0;$answer["i"] = 0;
                $answer["j"] = 0;$answer["n"] = 0;$answer["p"] = 0;
                $answer["s"] = 0;$answer["t"] = 0;
                for ($i = 0; $i < 60; $i++) {
                    if ($assessment->structure[$result[$i]["number"] - 1]->{"answer"}[$result[$i]["answer"] - 1]->{'index'} == "e") {
                        $answer["e"] = $answer["e"] + 1;
                    }
                    if ($assessment->structure[$result[$i]["number"] - 1]->{"answer"}[$result[$i]["answer"] - 1]->{'index'} == "f") {
                        $answer["f"] = $answer["f"] + 1;
                    }
                    if ($assessment->structure[$result[$i]["number"] - 1]->{"answer"}[$result[$i]["answer"] - 1]->{'index'} == "i") {
                        $answer["i"] = $answer["i"] + 1;
                    }
                    if ($assessment->structure[$result[$i]["number"] - 1]->{"answer"}[$result[$i]["answer"] - 1]->{'index'} == "j") {
                        $answer["j"] = $answer["j"] + 1;
                    }
                    if ($assessment->structure[$result[$i]["number"] - 1]->{"answer"}[$result[$i]["answer"] - 1]->{'index'} == "n") {
                        $answer["n"] = $answer["n"] + 1;
                    }
                    if ($assessment->structure[$result[$i]["number"] - 1]->{"answer"}[$result[$i]["answer"] - 1]->{'index'} == "p") {
                        $answer["p"] = $answer["p"] + 1;
                    }
                    if ($assessment->structure[$result[$i]["number"] - 1]->{"answer"}[$result[$i]["answer"] - 1]->{'index'} == "s") {
                        $answer["s"] = $answer["s"] + 1;
                    }
                    if ($assessment->structure[$result[$i]["number"] - 1]->{"answer"}[$result[$i]["answer"] - 1]->{'index'} == "t") {
                        $answer["t"] = $answer["t"] + 1;
                    }
                }
                $userService->start = 0 ;
                $userService->finish = 1 ;
                $userService->result = json_encode($answer);
                if ($userService->update() === false) {
                    $this->response->setStatusCode(406);
                    $this->response->setContent("we cant save result");
                    return $this->response;
                }else{
                    $this->response->setContent("you finish the assessment successfully");
                    return $this->response;
                }
            }
        } else {
            $this->response->setStatusCode(404);
            $this->response->setContent("user dont choose any service");
            return $this->response;
        }
    }

    public function assessmentResult($id)
    {
        $userService = Userservices::findFirst(
            [
                "conditions" => "id = :id:",
                "bind" => [
                    "id" => $id,
                ]
        ]);
        $result["user_id"] =$userService->user_id;
        $result["service_id"] =$userService->service_id;
        if (!$userService) {
            $this->response->setStatusCode(404);
            $this->response->setContent("Oops.. we cant find any result!");
            return $this->response;
        } else {
            $result["result"] = json_decode($userService->result);
            $this->response->setJsonContent(
                $result
            );
            return $this->response;
        }
    }
}
