<?php

use Phalcon\Mvc\Micro\Collection as MicroCollection;

return call_user_func(function () {
    $userServices = new MicroCollection();
    $userServices->setHandler(new \MyApp\Api\userServicesController\Controller());
    $userServices->setPrefix('/userServices');
    $userServices->get('/', 'getUserServices');
    $userServices->get('/{id}', 'getUserService');
    $userServices->get('/structure/{id}' ,'getAssessment');
    $userServices->get('/result/{id}' ,'assessmentResult');
    $userServices->post('/', 'createUserServices');
    $userServices->post('/assessment' , 'createAssessment');
    $userServices->post('/answer','answerAssessment');
    $userServices->put('/{id}', 'updateUserServices');
    $userServices->delete('/{id}', 'deleteUserServices');
    return $userServices;
});