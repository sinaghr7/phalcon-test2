<?php
 namespace MyApp\App\controllers;

 use MyApp\App\Models\Users;
 use Phalcon\Mvc\Controller;
 use Phalcon\Mvc\View;
class UserServiceController extends BaseController
{
    public function answerAction()
    {
        $token = $this->session->get('token');
        $user = new Users();
        $answers = $_POST["data"];
        $result = $user->getAnswer($token, $answers);
        $result = json_encode(json_decode($result->getBody())->data);
    }
    public function resultAction()
    {
//        $this->view->setRenderLevel(
//            View::LEVEL_ACTION_VIEW
//        );
        $this->assets->addCss('/public/css/chart.css' , true);
        $this->assets->addJs('https://cdn.jsdelivr.net/npm/chart.js@2.8.0', true);
        $this->assets->addJs('/public/js/chart.js', true);
        $token = $this->session->get('token');
        $user = new Users();
        $result = $user->getResult($token);
        $result = json_decode($result->getBody())->data;
        $this->view->result = $result;
    }
}