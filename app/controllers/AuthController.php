<?php

namespace MyApp\App\controllers;

use Phalcon\Mvc\Controller;
use MyApp\App\Models\Users;
use Phalcon\Mvc\View;

class AuthController extends controller
{

    public function loginAction()
    {
        $this->assets->addCss('/public/css/login/login.css', true);
        $this->assets->addJs('/public/js/auth/login/login.js', true);
        $this->view->setRenderLevel(
            View::LEVEL_ACTION_VIEW
        );
        if ($this->request->isPost() === true) {
            $user = new Users();
            $userName = $this->request->getPost('userName');
            $password = $this->request->getPost('password');
            $result = $user->login($userName, $password);
            $token = json_encode(json_decode($result->getBody())->data);
            if ($result->getStatusCode() == 406 || $result->getStatusCode() === 404){
                $response = json_encode(json_decode($result->getBody())->message);
                $this->view->setVar('username', $userName);
                $this->view->setVar('error', $response);
            }else{
                $session = $user->getSession($token);
                $test = json_decode($session->getBody())->data;
                $this->session->set('token', $token);
                $this->session->set('id', $test->id);
                $this->session->set('FirstName', $test->FirstName);
                $this->session->set('LastName', $test->LastName);
                $this->session->set('userName', $test->userName);
                $this->session->set('password', $test->password);
                $this->session->set('role', $test->role);
                return $this->response->redirect("/");
            }
        } else {
            return true;
        }
    }

    public function registerAction()
    {
        $this->assets->addCss('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css', true);
        $this->assets->addCss('/public/css/register.css', true);
        $this->view->setRenderLevel(
            View::LEVEL_ACTION_VIEW
        );
        if ($this->request->isPost()) {
            $user = new Users();
            $userName = $this->request->getPost('username');
            $password = $this->request->getPost('password');
            $FirstName = $this->request->getPost('FirstName');
            $LastName = $this->request->getPost('LastName');
            $result = $user->register($userName, $password, $FirstName, $LastName);
            $token = json_encode(json_decode($result->getBody())->data);
            if ($result->getStatusCode() == 406){
                $response = json_encode(json_decode($result->getBody())->message);
                $this->view->setVar('firstName', $FirstName);
                $this->view->setVar('lastName', $LastName);
                $this->view->setVar('error', $response);
            }else{
                $session = $user->getSession($token);
                $test = json_decode($session->getBody())->data;
                $this->session->set('token', $token);
                $this->session->set('id', $test->id);
                $this->session->set('FirstName', $test->FirstName);
                $this->session->set('LastName', $test->LastName);
                $this->session->set('userName', $test->userName);
                $this->session->set('password', $test->password);
                $this->session->set('role', $test->role);
                return $this->response->redirect("/");
            }
        } else {
            return true;
        }
    }
}