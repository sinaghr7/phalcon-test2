<?php

namespace MyApp\App\Plugin;

use Phalcon\Mvc\User\Plugin;
use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;

class SecurityPlugin extends Plugin
{
    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
    {
        $controller = $dispatcher->getControllerName();
        $action     = $dispatcher->getActionName();
        if (!$this->session->has('token')) {
            if ($controller != "auth" && $action != "login") {
                if ($controller == "auth" && $action =="login") {
                    $this->flashSession->error('Please login first :)');
                    $dispatcher->forward(
                        [
                            'controller' => 'auth',
                            'action' => 'login',
                        ]
                    );
                }else{
                    $this->flashSession->error('Please login first :)');
                    $dispatcher->forward(
                        [
                            'controller' => 'auth',
                            'action' => 'login',
                        ]
                    );
                }
                return false;
            }
        } else {
            if ($controller != "auth" && $action != "login") {
                return true;
            }else {
                $dispatcher->forward(
                    [
                        'controller' => 'index',
                        'action' => 'index',
                    ]
                );
            }
        }
    }
}