<?php

namespace MyApp\Middleware\Plugin;

use Phalcon\Acl;
use Phalcon\Acl\Role;
use Phalcon\Acl\Resource;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;
use Phalcon\Acl\Adapter\Memory as AclList;
use Phalcon\Di\Injectable;

class AclPlugin extends Injectable implements MiddlewareInterface
{
    public function call(Micro $app)
    {
        $acl = new AclList();
        $acl->setDefaultAction(Acl::DENY);
        $roleAdmins = new Role('Admins');
        $roleUsers = new Role('Users');
        $roleGuests = new Role('Guests');

        $acl->addRole($roleAdmins);
        $acl->addRole($roleUsers);
        $acl->addRole($roleGuests);

        $acl->addInherit($roleUsers, $roleGuests);
        $acl->addInherit($roleAdmins, $roleUsers);

        $AdminsResource = new Resource('Admins');
        $UsersResource = new Resource('Users');
        $GuestsResource = new Resource('Guests');

        $acl->addResource(
            $GuestsResource,
            ['registerUser', 'loginUser']);
        $acl->addResource(
            $UsersResource,
            ['registerUser', 'loginUser',
                'getUsers', 'getUser',
                'updateUser', 'deleteUser']);
        $acl->addResource(
            $AdminsResource,
            ['getUsers', 'getUser', 'registerUser', 'loginUser', 'updateUser', 'deleteUser', 'getServices',
                'getService', 'createService', 'updateService', 'deleteService', 'getUserServices',
                'getUserService', 'createUserServices', 'updateUserServices', 'deleteUserServices', 'getAssessment'
                , 'createAssessment', 'answerAssessment', 'assessmentResult']);

        $adminsAllows = array('getUsers', 'getUser', 'registerUser', 'loginUser', 'updateUser', 'deleteUser', 'getServices',
            'getService', 'createService', 'updateService', 'deleteService', 'getUserServices',
            'getUserService', 'createUserServices', 'updateUserServices', 'deleteUserServices', 'getAssessment'
            , 'createAssessment', 'answerAssessment', 'assessmentResult');
        $usersAllows = array('getUsers', 'getUser', 'registerUser', 'loginUser', 'updateUser', 'deleteUser');
        $guestsAllows = array('registerUser', 'loginUser');

        foreach ($adminsAllows as $action) {
            $acl->allow('Admins', 'Admins', $action);
        }

        foreach ($usersAllows as $action) {
            $acl->allow('Users', 'Users', $action);
        }
        foreach ($guestsAllows as $action) {
            $acl->allow('Guests', 'Guests', $action);
        }

        //isAllow
        $userRole = $this->di->get("userRole");
        $handler = $app->getActiveHandler();
        $access = $acl->isAllowed($userRole, $userRole, $handler[1]);
        if (!$access) {
            $app->response->setStatusCode(401);
            $app->response->setContent("You dont have access")->send();
            return $app->stop();
        } else {
            return true;
        }
    }
}


