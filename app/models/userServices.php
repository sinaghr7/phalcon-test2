<?php
namespace MyApp\Models\UserServices;

use Phalcon\Mvc\Model;

class Userservices extends Model
{
    public $id;
    public $user_id;
    public $service_id;
    
    public function initialize()
    {
        $this->belongsTo('user_id' , 'Users' , 'id');
        $this->belongsTo('service_id' , 'Services' , 'id');
    }
}