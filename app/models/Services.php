<?php
namespace MyApp\App\Models;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class Services
{
    public $client;
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'http://phalconApi',
        ]);
    }
    public function getService($token ,$id)
    {
        try {
            return $this->client->get('/services/getService',['headers' => ['token' => $token],
                'json' => [
                    'service_id' => $id
                ]]);
        } catch (BadResponseException $e) {
            return $e->getResponse();
        }
    }
    public function updateService($token , $id,$title,$description,$order,$price,$assessment_id)
    {
        try {
            return $this->client->put('/services/update',['headers' => ['token' => $token] ,
                'json' => [
                    'service_id' => $id,
                    'title' => $title,
                    'description' => $description,
                    'order' => $order,
                    'price' => $price,
                    'assessment_id' => $assessment_id,
                ]]);
        } catch (BadResponseException $e) {
            return $e->getResponse();
        }
    }
    public function deleteService($token, $id)
    {
        $url = '/services/delete' ;
        try {
            return  $this->client->delete($url ,[
                'headers' => ['token' => $token],
                'json' => [
                    'service_id' => $id,
                ]
            ]);
        } catch (BadResponseException $e) {
            return $e->getResponse();
        }
    }
}