<!DOCTYPE html>
<html lang="">
<head>

    <meta charset="UTF-8">

    <title>Register</title>
    <style type="text/css">
        label {
            color: white;
        }

        .alert {
            padding: 20px;
            background-color: rgb(231, 40, 40);
            border-radius: 10px;
            color: white;
            width: 400px;
            margin: 20px auto 10px;
        }

        .closebtn {
            margin-left: 15px;
            color: white;
            font-weight: bold;
            float: right;
            font-size: 22px;
            line-height: 20px;
            cursor: pointer;
            transition: 0.3s;
        }

        .closebtn:hover {
            color: black;
        }
    </style>
    {{ assets.outputCss() }}

</head>

<body>

<div id="logo">
    <h1><i> STARK LOGIN</i></h1>
</div>
<section class="stark-login">
    <div class="center">
        <div class="wrapper">
            <form {% if error is defined %}
                class="was-validated"
            {% endif %} action="/auth/login" method="post">
                <div id="fade-box">
                    <div>
                        <input type="text" class="form-control" name="userName" required placeholder="Username"
                                {% if username is defined %}
                            value="{{ username }}"
                                {% endif %}>
                        <span class="help-block"></span>
                    </div>
                    <div>
                        <input type="password" class="form-control" name="password" required placeholder="Password">
                        {% if error is defined %}
                            <div class="alert">
                                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                                Username or password is wrong
                            </div>
                        {% endif %}
                        <span class="help-block"></span>
                    </div>

                    <input type="submit" value="Sign In">
                    <a href="/auth/register"><i class="fa fa-user"></i>Create an Account</a>
                </div>
            </form>
        </div>
    </div>
    <div class="hexagons">
        <img src="/public/img/NX-Desktop-BG.png" height="768px"
             width="1366px"/>
    </div>
</section>

<div id="circle1">
    <div id="inner-cirlce1">
        <h2></h2>
    </div>
</div>

<ul>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
</ul>
{{ assets.outputJs() }}

</body>

</html>