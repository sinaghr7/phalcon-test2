<!doctype html>
<html lang="en">
<head>
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">
    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
    <style type="text/css">
        body {
            font: 14px sans-serif;
        }

        .text {
            text-align: center;
            font: 20px sans-serif;
            font-weight: bold;
        }
    </style>
</head>
<body>
<header>
    <div class="navbar navbar-dark bg-dark box-shadow">
        <div class="container d-flex justify-content-between">
            <a href="/index/index" class="navbar-brand d-flex align-items-center">
                <div>
                    <svg width="20" height="20" viewBox="0 0 19 19" class="bi bi-house" fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
                        <path fill-rule="evenodd"
                              d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/>
                    </svg>
                </div>
                <strong>Home</strong>
            </a>
            <a href="/dashboard/logout" class="navbar-brand d-flex align-items-center">
                <svg width="25" height="17" viewBox="0 0 16 16" class="bi bi-box-arrow-left" fill="currentColor"
                     xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                          d="M6 12.5a.5.5 0 0 0 .5.5h8a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-8a.5.5 0 0 0-.5.5v2a.5.5 0 0 1-1 0v-2A1.5 1.5 0 0 1 6.5 2h8A1.5 1.5 0 0 1 16 3.5v9a1.5 1.5 0 0 1-1.5 1.5h-8A1.5 1.5 0 0 1 5 12.5v-2a.5.5 0 0 1 1 0v2z"/>
                    <path fill-rule="evenodd"
                          d="M.146 8.354a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L1.707 7.5H10.5a.5.5 0 0 1 0 1H1.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3z"/>
                </svg>
                <strong>Logout </strong>
            </a>
        </div>
    </div>
</header>

<main role="main">
    <div>
        <div class="text">Users</div>
        <table class="table table-striped table-dark">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">User name</th>
                <th scope="col">First name</th>
                <th scope="col">Last name</th>
                <th scope="col">Role</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($users as $user) { ?>
                <tr>
                    <th scope="row"><?= $user->id ?></th>
                    <td><?= $user->userName ?></td>
                    <td><?= $user->FirstName ?></td>
                    <td><?= $user->LastName ?></td>
                    <td><?= $user->role ?></td>
                    <td><a class="btn btn-outline-warning" href="/dashboard/editUser/<?= $user->id ?>" role="button">Edit</a> <a
                                class="btn btn-outline-danger" href="/dashboard/deleteUser/<?= $user->id ?>" role="button">Delete</a></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <div>
        <div class="text">Services</div>
        <table class="table table-striped table-dark">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">title</th>
                <th scope="col">description</th>
                <th scope="col">order</th>
                <th scope="col">price</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($services as $service) { ?>
                <tr>
                    <th scope="row"><?= $service->id ?></th>
                    <td><?= $service->title ?></td>
                    <td><?= $service->description ?></td>
                    <td><?= $service->order ?></td>
                    <td><?= $service->price ?></td>
                    <td><a class="btn btn-outline-warning" href="/dashboard/editService/<?= $service->id ?>" role="button">Edit</a> <a
                                class="btn btn-outline-danger" href="/dashboard/deleteService/<?= $service->id ?>" role="button">Delete</a></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</main>
</body>
</html>

<script>
    console.log("asDSAds");
</script>

