<main role="main" class="inner cover">
    <h1 class="cover-heading">{{ service.title }}</h1>
    <p class="lead">{{ service.description }}.</p>
    <p class="lead">
        {% if result is defined %}
            <a href="/userservice/result" class="btn btn-lg btn-secondary">Result</a>
        {% endif %}
        {% if result is not defined %}
            <a href="/services/mbti" class="btn btn-lg btn-secondary">Start the assessment</a>
        {% endif %}
    </p>
</main>


