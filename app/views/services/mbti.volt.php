<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Assessment</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body {
            font: 14px sans-serif;
        }

        .wrapper {
            width: 350px;
            padding: 20px;
        }

        .center {
            margin: auto;
            width: 60%;
            border: 5px solid #ffff00;
            padding: 20px;
        }
    </style>
</head>
<body>
<div class="center" id="Question_id">
    <div class="wrapper">
        <h2 id="number"></h2>
        <h3>کدام گزینه را انتخاب میکنید؟</h3>
        <div class="row">
            <div class="col-4">
                <div class="list-group" id="question" role="tablist">

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    let assessment = <?= $assessment ?>;
    let assessment_id = <?= $assessment_id ?>;
</script>
<?= $this->assets->outputJs() ?>
</body>
</html>
