<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
<canvas id="myChart"></canvas>

<script  type="">
    let result_e = <?= $result->result->e ?>;
    let result_f = <?= $result->result->f ?>;
    let result_i = <?= $result->result->i ?>;
    let result_j = <?= $result->result->j ?>;
    let result_n = <?= $result->result->n ?>;
    let result_p = <?= $result->result->p ?>;
    let result_s = <?= $result->result->s ?>;
    let result_t = <?= $result->result->t ?>;
</script>
<?= $this->assets->outputJs() ?>
</body>
</html>

