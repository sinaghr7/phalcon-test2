<?php
use Phalcon\Mvc\Router;

$router = new Router(true);

// Define a route
$router->setDefaults([
    "controller" => "index" ,
    "action" => "index",
]);
$router->add(
    '/:controller',
    [
        'controller' => 1,
    ]
);
$router->add(
    '/:controller/:action',
    [

        'controller' => 1,
        'action' => 2,
    ]
);
$router->add(
    '/:controller/:action/:params',
    [
        'controller' => 1,
        'action'     => 2,
        'params' => 3,
    ]
);
return $router;