<?php

use Phalcon\Di\FactoryDefault;
use \MyApp\App\Plugin\SecurityPlugin;
use Phalcon\Session\Adapter\Files as Session;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Volt;
use Phalcon\Mvc\Router;
use Phalcon\Flash\Direct as FlashDirect;
use Phalcon\Flash\Session as FlashSession;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Events\Manager as EventsManager;

//database
$di = new FactoryDefault();
$di->set(
    'router',
    function () {
        return require_once(__DIR__.DIRECTORY_SEPARATOR.'routes.php');
    }
);
$di->set(
    'voltService',
    function ($view, $di) {
        $volt = new Volt($view, $di);
        $volt->setOptions(
            [
                'compiledPath' =>  __DIR__.DIRECTORY_SEPARATOR.'../app/voltCompilePath/',
                'compiledExtension' => '.compiled',
            ]
        );
        return $volt;
    }
);
$di->set(
    'view',
    function () {
        $view = new View();
        $view->setViewsDir('../app/views');
        $view->registerEngines(
            [
                '.volt' => Phalcon\Mvc\View\Engine\Volt::class,
            ]
        );
        $view->setLayoutsDir(__DIR__ . DIRECTORY_SEPARATOR. '../views/layout/main.volt');
        $view->setLayout('main');
        return $view;
    }
);
$di->setShared(
    'session',
    function () {
        $session = new Session();

        $session->start();

        return $session;
    }
);
$di->set(
    'url',
    function() {
        $url = new \Phalcon\Mvc\Url();
        $url->setBaseUri('/');
        return $url;
    }
);
$di->set(
    'flash',
    function () {
        return new FlashDirect();
    }
);
$di->set(
    'flashSession',
    function () {
        return new FlashSession();
    }
);
$di->set(
    'dispatcher',
    function () {
        $eventsManager = new EventsManager();
        $eventsManager->attach(
            'dispatch:beforeExecuteRoute',
            new SecurityPlugin()
        );
        $dispatcher = new Dispatcher();
        $dispatcher->setEventsManager($eventsManager);
        $dispatcher->setDefaultNamespace('MyApp\App\controllers');
        return $dispatcher;
    }
);


