new Vue({
    el: '#app',
    delimiters: ['[[',']]'],
    data() {
        return {
            questions: assessment,
            questionNumber: 0,
            answers: []
        }
    },
    computed: {
        getAnswers() {
            return JSON.stringify(this.answers)
        },
        question() {
            return this.questions[this.questionNumber]
        }
    },
    methods: {
        nextPage: function (answer) {
            if (this.questionNumber < 59) {
                this.answers.push({
                    'number': this.questionNumber,
                    'answer': answer
                })
                this.questionNumber = this.questionNumber + 1
            } else {
                this.$refs.questionsForm.submit()
            }
        }
    }
})

