//print Question
// {
let dataStructure = assessment; //todo
var assessmentId = [];
if (localStorage.getItem(assessment_id.toString()) === null) {
    localStorage.setItem(assessment_id.toString(), assessment_id);
    localStorage.setItem(assessment_id, JSON.stringify(assessmentId));
    var currentQuestionNumber = dataStructure[0].number;
} else {
    var result = JSON.parse(localStorage.getItem(assessment_id))
    var currentQuestionNumber = result.length + 1;
}
const loadTheCorrectQuestion = (currentQuestionNumber) => {
    //todo : load the question number into DIV
    var x = ' <label for="indoor"><input id="indoor" type="radio" name="indoor-outdoor" value=1> ' + dataStructure[currentQuestionNumber - 1].answer[0] + '</label>' +
        '<br> <label for="outdoor"><input id="outdoor"  type="radio" name="indoor-outdoor" value=2> ' + dataStructure[currentQuestionNumber - 1].answer[1] + '</label>' +
        '<br><br> <button type="submit" onclick="handleAnswers()" >Next Page</button>';
    $("#number").html(dataStructure[currentQuestionNumber - 1].number);
    $("#question").html(x);
}
const handleAnswers = () => {
    //todo : save the answer and load the next question
    var answer = JSON.parse(localStorage.getItem(assessment_id));
    if ($("#indoor").is(":checked")) {
        answer.push({
            'number': currentQuestionNumber,
            'answer': 1
        })
    }
    if ($("#outdoor").is(":checked")) {
        answer.push({
            'number': currentQuestionNumber,
            'answer': 2
        })
    }
    localStorage.setItem(assessment_id, JSON.stringify(answer));
    if (currentQuestionNumber < 60) {
        if ($("#indoor").is(":checked") || $("#outdoor").is(":checked")) {
            currentQuestionNumber = JSON.parse(localStorage.getItem(assessment_id))
            currentQuestionNumber = currentQuestionNumber.length + 1
            loadTheCorrectQuestion(currentQuestionNumber);
        }
    } else {
        alert("you finish the assessment");
        var data = JSON.parse(localStorage.getItem(assessment_id));
        $.ajax({
            type: "POST",  // http method
            url: "/user-service/answer",
            data: {data: data}   // data to submit
        }).then(function (x) {
            window.location.href = "/userservice/result";
            console.log("finish");
        })
    }
};
loadTheCorrectQuestion(currentQuestionNumber);
// }

