var ctx = document.getElementById('myChart').getContext('2d');
var myRadarChart = new Chart(ctx, {
    type: 'radar',
    data: {
        labels: ['e', 'f', 'i', 'j', 'n', 'p', 's', 't'],
        datasets: [{
            label: 'My First dataset',
            backgroundColor: 'rgba(0 , 0 , 0 , 0.1)',
            borderColor: 'rgba(0, 0, 0, 0.1)',
            data: [result_e, result_f, result_i, result_j, result_n, result_p, result_s , result_t]
        }]
    },
    options: {
        legend: {
            display: false,
        },
    }
});
// var chart = new Chart(ctx, {
//     // The type of chart we want to create
//     type: 'line',
//     // The data for our dataset
//     data: {
//         labels: ['e', 'f', 'i', 'j', 'n', 'p', 's', 't'],
//         datasets: [{
//             label: 'My First dataset',
//             backgroundColor: 'rgb(255, 99, 132)',
//             borderColor: 'rgb(255, 99, 132)',
//             data: [result_e, result_f, result_i, result_j, result_n, result_p, result_s , result_t]
//         }]
//     },
//
//     // Configuration options go here
//     options: {}
// });
