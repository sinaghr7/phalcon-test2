<?php

use Phalcon\Mvc\Micro;
use MyApp\Middleware\Auth\AuthMiddleware;
use MyApp\Middleware\Acl\AclPlugin;
use MyApp\Middleware\acl\UserRole;

try {
    //loader
    require_once('../config/loader.php');
    //database
    require_once('../config/di.php');
    // Users handler
    $user = require_once('../api/users/router.php');
    $service = require_once('../api/services/router.php');
    $userServices = require_once('../api/userServices/router.php');
    $auth = new AuthMiddleware();
    $acl = new AclPlugin();
    $app = new Micro();
    $app->setDI($di);
    $app->mount($user);
    $app->mount($service);
    $app->mount($userServices);
    $app->before($auth);
    $app->before($acl);
    $app->handle();
} catch (\Exception $e) {
    var_dump($e);
}
